// @flow
import React from 'react';
import { Header } from 'semantic-ui-react'

type TProps = {
  content: string,
  className: string
};

const Title = ({ content, className }: TProps) => (
    <Header content={content} className={className} />
);

export default Title;
