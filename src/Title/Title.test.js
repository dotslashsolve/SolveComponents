import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Title from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('Title', () => {
    it('should render without errors', () => {
        shallow(<Title content="123" />);
    });
});
